/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.ashish29agre.springboottuts.controllers;

import in.ashish29agre.springboottuts.models.Todo;
import in.ashish29agre.springboottuts.repositories.TodoRepository;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Ashish
 */
@RestController
@RequestMapping("/todo")
public class TodoController {

    @Autowired
    private TodoRepository todoRepository;

    @RequestMapping(method = {RequestMethod.POST})
    public Map<String, Object> create(@RequestBody Map<String, String> body) {
        Map<String, Object> response = new HashMap<>();
        Todo todo = new Todo(body.get("item"));
        response.put("msg", "Todo created successfully");
        response.put("todo", todoRepository.save(todo));
        return response;
    }

    @RequestMapping(method = RequestMethod.DELETE, value="/{todoId}")
    public Map<String, Object> delete(@PathVariable("todoId") String todoId) {
        todoRepository.delete(todoId);
        Map<String, Object> response = new HashMap<>();
        response.put("msg", "Todo deleted successfully");
        return response;
    }
    
    @RequestMapping(value = {"/all"}, method = {RequestMethod.GET})
    public Map<String, Object> getAll() {
        List<Todo> allItems = todoRepository.findAll();
//        return Response.(allItems).build();
        Map<String, Object> response = new HashMap<String, Object>();
        response.put("count", allItems.size());
        response.put("items", allItems);
        return response;
    }
}
