/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.ashish29agre.springboottuts.controllers;

import java.util.HashMap;
import java.util.Map;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Ashish
 */
@RestController
@RequestMapping("/hello")
public class HelloController {

    @RequestMapping(method = {RequestMethod.GET})
    public Map<String, String> sayHello() {
        Map<String, String> helloMessage = new HashMap<>();
        helloMessage.put("msg", "Hello Ashish!");
        return helloMessage;
    }
}
